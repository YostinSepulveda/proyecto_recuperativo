#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class dialogo_info_main():

    # metodo constructor
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./ui/window.ui")

        # Ventana info

        self.window_info = builder.get_object("window_info")
        self.window_info.resize(200,250)
        self.window_info.show_all()
    

        # Aceptar info
        self.aceptar_info = builder.get_object("btn_final")
        self.aceptar_info.connect("clicked", self.on_btn_aceptar)

    def on_btn_aceptar(self,btn=None):
        self.window_info.destroy()
