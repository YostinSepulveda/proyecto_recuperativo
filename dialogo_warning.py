#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dialogo_info import dialogo_info_main


class dialogo_warning:

    # metodo constructor
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./ui/window.ui")
        # Ventana warning

        self.window_warning = builder.get_object("window_warning")
        self.window_warning.show_all()

        # boton aceptar

        self.aceptar_warning = builder.get_object("aceptar_warning")
        self.aceptar_warning.connect("clicked", self.on_btn_aceptar_warning)   
        
        # boton cancelar

        self.cancelar_warning = builder.get_object("cancelar_warning")
        self.cancelar_warning.connect("clicked", self.on_btn_cancelar_warning)   
    
    def on_btn_aceptar_warning(self, btn=None):
        self.texto1 = self.texto1.get_text("")
        self.texto2 = self.texto2.get_text("")
        self.window_warning.destroy()
    def on_btn_cancelar_warning(self, btn=None):
        self.window_warning.destroy()
