#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dialogo_info import dialogo_info_main
from dialogo_warning import dialogo_warning


class MainWindow():

    # metodo constructor
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./ui/window.ui")

        # Ventana principal
        window = builder.get_object("principal")
        window.connect("destroy", Gtk.main_quit)
        window.set_title("Proyecto")
        window.maximize()
        window.show_all()

        # Cuadro de texto 1
        self.texto1 = builder.get_object("entry_text1")

        #Label 1
        self.label_1 = builder.get_object("label_f1")

        # Cuadro de texto 2
        self.texto2 = builder.get_object("entry_text2")

        #Label 2
        self.label_2 = builder.get_object("label_f2")

        # Label Resultados
        self.resultados = builder.get_object("label_resultados")

        # boton aceptar
        boton_aceptar = builder.get_object("btn_aceptar")
        boton_aceptar.connect("clicked", self.on_btn_aceptar_click)
        boton_aceptar.set_label("Aceptar")

        # boton reiniciar
        boton_reiniciar = builder.get_object("btn_reiniciar")
        boton_reiniciar.connect("clicked", self.on_btn_reiniciar_click)
        boton_reiniciar.set_label("Reiniciar")

        #identificar datos entregados



    def on_btn_aceptar_click(self,btn=None):

        dlg = dialogo_info_main()
        response = dlg.window_info.run()

        texto1 = self.texto1.get_text()
        self.label_1.set_text(texto1)
        texto2 = self.texto2.get_text()
        self.label_2.set_text(texto2)



    def on_btn_reiniciar_click(self,btn=None):
        
        dlg = dialogo_warning()
        response = dlg.window_warning.run()
        

if __name__ == "__main__":
    MainWindow()
    Gtk.main()